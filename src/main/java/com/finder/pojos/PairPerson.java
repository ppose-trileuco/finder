package com.finder.pojos;

import java.util.List;
import java.util.Set;

public class PairPerson {

    private Person personWithMinBirthDate;

    private Person personWithMaxBirthDate;

    public PairPerson() {
        super();
    }

    public PairPerson(Person personWithMinBirthDate, Person personWithMaxBirthDate) {
        super();
        this.personWithMinBirthDate = personWithMinBirthDate;
        this.personWithMaxBirthDate = personWithMaxBirthDate;
    }

    public Person getPersonWithMinBirthDate() {
        return personWithMinBirthDate;
    }

    public void setPersonWithMinBirthDate(Person personWithMinBirthDate) {
        this.personWithMinBirthDate = personWithMinBirthDate;
    }

    public Person getPersonWithMaxBirthDate() {
        return personWithMaxBirthDate;
    }

    public void setPersonWithMaxBirthDate(Person personWithMaxBirthDate) {
        this.personWithMaxBirthDate = personWithMaxBirthDate;
    }

    public long getDiffBirthDates() {
        return personWithMinBirthDate.getBirthDate().toEpochDay() - personWithMaxBirthDate.getBirthDate().toEpochDay();
    }

    public boolean haveCloserBirthDate(PairPerson otherPairPerson) {
        if (otherPairPerson == null) {
            return true;
        }
        return getDiffBirthDates() > otherPairPerson.getDiffBirthDates();
    }

    public static PairPerson buildFromPersonPairCombination(Set<Person> personCombination) {
        List<Person> sortedPersonsByBirthDate = Person.getSortedByBirthDate(personCombination);
        Person personWithMinBrithDate = sortedPersonsByBirthDate.get(0);
        Person personWhitMaxBrithDate = sortedPersonsByBirthDate.get(1);
        return new PairPerson(personWithMinBrithDate, personWhitMaxBrithDate);
    }

}
