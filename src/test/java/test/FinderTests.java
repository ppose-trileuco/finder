package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.finder.algorithm.BirthDateFinder;
import com.finder.pojos.PairPerson;
import com.finder.pojos.Person;

public class FinderTests {

    Person sue = new Person();
    Person greg = new Person();
    Person sarah = new Person();
    Person mike = new Person();

    @Before
    public void setup() {
        sue.setName("Sue");
        sue.setBirthDate(LocalDate.of(1950, Month.JANUARY, 1));
        greg.setName("Greg");
        greg.setBirthDate(LocalDate.of(1951, Month.MAY, 1));
        sarah.setName("Sarah");
        sarah.setBirthDate(LocalDate.of(1982, Month.JANUARY, 1));
        mike.setName("Mike");
        mike.setBirthDate(LocalDate.of(1979, Month.JANUARY, 1));
    }

    @Test
    public void ReturnsEmptyResultsWhenGivenEmptyList() {
        List<Person> list = new ArrayList<Person>();

        PairPerson result = BirthDateFinder.findPersonsWithClosestBirthDates(list);
        assertEquals(null, result.getPersonWithMinBirthDate());

        assertEquals(null, result.getPersonWithMaxBirthDate());
    }

    @Test
    public void ReturnsEmptyResultsWhenGivenOnePerson() {
        List<Person> list = new ArrayList<Person>();
        list.add(sue);

        PairPerson result = BirthDateFinder.findPersonsWithClosestBirthDates(list);

        assertEquals(null, result.getPersonWithMinBirthDate());
        assertEquals(null, result.getPersonWithMaxBirthDate());
    }

    @Test
    public void ReturnsClosestTwoForTwoPeople() {
        List<Person> list = new ArrayList<Person>();
        list.add(sue);
        list.add(greg);

        PairPerson result = BirthDateFinder.findPersonsWithClosestBirthDates(list);

        assertEquals(sue, result.getPersonWithMinBirthDate());
        assertEquals(greg, result.getPersonWithMaxBirthDate());
    }

    @Test
    public void ReturnsFurthestTwoForTwoPeople() {
        List<Person> list = new ArrayList<Person>();
        list.add(mike);
        list.add(greg);

        PairPerson result = BirthDateFinder.findPersonsWithFurthestBirthDates(list);

        assertEquals(greg, result.getPersonWithMinBirthDate());
        assertEquals(mike, result.getPersonWithMaxBirthDate());
    }

    @Test
    public void ReturnsFurthestTwoForFourPeople() {
        List<Person> list = new ArrayList<Person>();
        list.add(sue);
        list.add(sarah);
        list.add(mike);
        list.add(greg);

        PairPerson result = BirthDateFinder.findPersonsWithFurthestBirthDates(list);

        assertEquals(sue, result.getPersonWithMinBirthDate());
        assertEquals(sarah, result.getPersonWithMaxBirthDate());
    }

    @Test
    public void ReturnsClosestTwoForFourPeople() {
        List<Person> list = new ArrayList<Person>();
        list.add(sue);
        list.add(sarah);
        list.add(mike);
        list.add(greg);

        PairPerson result = BirthDateFinder.findPersonsWithClosestBirthDates(list);

        assertEquals(sue, result.getPersonWithMinBirthDate());
        assertEquals(greg, result.getPersonWithMaxBirthDate());
    }

}
