package com.finder.pojos;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

public class Person {

    private String name;

    private LocalDate birthDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public static List<Person> getSortedByBirthDate(Collection<Person> persons) {
        if (persons == null) {
            return null;
        }
        return persons.stream().sorted(comparing(Person::getBirthDate)).collect(toList());
    }

}
