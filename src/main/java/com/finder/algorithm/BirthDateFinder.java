package com.finder.algorithm;

import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Iterables.getLast;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

import java.util.List;
import java.util.Set;

import com.finder.pojos.PairPerson;
import com.finder.pojos.Person;
import com.google.common.collect.Sets;

public class BirthDateFinder {

    private static int MIN_PERSONS_FOR_FIND = 2;

    private static int SIZE_PER_COMBINATION = 2;

    private static boolean isValidNumPersons(List<Person> persons) {
        return !isEmpty(persons) && persons.size() >= MIN_PERSONS_FOR_FIND;
    }

    public static PairPerson findPersonsWithFurthestBirthDates(List<Person> persons) {

        PairPerson pairPerson = new PairPerson();
        if (!isValidNumPersons(persons)) {
            return pairPerson;
        }

        List<Person> sortedPersonsByBirthDate = Person.getSortedByBirthDate(persons);

        Person personWithMinBrithDate = getFirst(sortedPersonsByBirthDate, null);
        pairPerson.setPersonWithMinBirthDate(personWithMinBrithDate);

        Person personWithMaxBrithDate = getLast(sortedPersonsByBirthDate);
        pairPerson.setPersonWithMaxBirthDate(personWithMaxBrithDate);

        return pairPerson;

    }

    public static PairPerson findPersonsWithClosestBirthDates(List<Person> persons) {

        if (!isValidNumPersons(persons)) {
            return new PairPerson();
        }

        PairPerson pairPersonWithClosertBirthDates = null;
        Set<Set<Person>> personCombinations = Sets.combinations(Sets.newHashSet(persons), SIZE_PER_COMBINATION);

        for (Set<Person> personCombination : personCombinations) {
            PairPerson pairPerson = PairPerson.buildFromPersonPairCombination(personCombination);
            if (pairPerson.haveCloserBirthDate(pairPersonWithClosertBirthDates)) {
                pairPersonWithClosertBirthDates = pairPerson;
            }
        }

        return pairPersonWithClosertBirthDates;
    }

}
